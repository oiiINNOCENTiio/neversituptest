export const state = () => ({
  token: "",
  todos: [],
});

export const mutations = {
  setToken(state, data) {
    state.token = data;
  },
  setTodos(state, data) {
    state.todos = data;
  },
};

export const actions = {
  async doGetToDos({ commit, state }) {
    const todos = await this.$axios.$get(`${process.env.api}/todo/todos`, {
      headers: {
        Authorization: `Bearer ${state.token}`,
      },
    });
    console.log(todos);
    commit("setTodos", todos);
  },
  async createTodo({ commit, state }, param) {
    await this.$axios.$post(`${process.env.api}/todo/todos`, param, {
        headers: {
            Authorization: `Bearer ${state.token}`,
        }
    });
  },
  async editTodo({commit, state}, id, param){
    await this.$axios.$put(`${process.env.api}/todo/todos/${id}`, param, {
      headers: {
        Authorization: `Bearer ${state.token}`,
      }
    })
  },
  async deleteTodo({commit, state}, id) {
    await this.$axios.$delete(`${process.env.api}/todo/todos/${id}`, {
        headers: {
            Authorization: `Bearer ${state.token}`,
        }
    })
  }
};

export const getters = {
  getToken(state) {
    return state.token;
  },
  getTodos(state) {
    return state.todos;
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};
